package com.example.androiddemo;
import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.FragmentTransaction;

import android.widget.Button;
import android.widget.MediaController;
import android.widget.RadioGroup;
import android.widget.VideoView;

import com.example.androiddemo.fragments.FxFragment;
import com.example.androiddemo.fragments.TxlFragment;
import com.example.androiddemo.fragments.WxFragment;



import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;
public class MainActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {

        WxFragment wxFragment = null;
        TxlFragment txlFragment = null;
        FxFragment fxFragment = null;
        RadioGroup rg_tabbar;

        Button end;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_main);

                rg_tabbar = findViewById(R.id.rg_tabbar);
                rg_tabbar.setOnCheckedChangeListener(this);
                if(wxFragment == null){
                        wxFragment = new WxFragment();
                }


//        FragmentManager manager = getSupportFragmentManager();
//        FragmentTransaction transaction = manager.beginTransaction();
//        transaction.replace(R.id.fl_content,wxFragment);
//        transaction.commit();

                getSupportFragmentManager().beginTransaction().replace(R.id.fl_content,wxFragment).commit();

        }




                @Override
        public void onCheckedChanged(RadioGroup radioGroup, int id) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                if(id == R.id.rb_wx){
                        if(wxFragment == null){
                                wxFragment = new WxFragment();
                        }
                        transaction.replace(R.id.fl_content,wxFragment);
                }
                if(id == R.id.rb_txl){
                        if(txlFragment == null){
                                txlFragment = new TxlFragment();
                        }
                        transaction.replace(R.id.fl_content,txlFragment);

                }
                if(id == R.id.rb_fx){
                        if(fxFragment == null){
                                fxFragment = new FxFragment();
                        }
                        transaction.replace(R.id.fl_content,fxFragment);

                }

                transaction.commit();
        }
}
