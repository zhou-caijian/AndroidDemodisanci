package com.example.androiddemo;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import android.os.Build;

import androidx.appcompat.app.AppCompatActivity;


public class register extends AppCompatActivity {

    //下面是两个显示时间的常量
    public static final int LENGTH_SHORT = 0;
    public static final int LENGTH_LONG = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Button bt_logon = (Button)findViewById(R.id.logon);
        Button bt_land = (Button)findViewById(R.id.land);

        final CheckBox remember = (CheckBox)findViewById(R.id.checkBox1);
        final EditText usr = (EditText)findViewById(R.id.usename);
        final EditText password = (EditText)findViewById(R.id.password);

        //准备写入对象
        final SharedPreferences mysp = getSharedPreferences("passport", MODE_PRIVATE);

        //根据自动填入设置填入账号和密码
        boolean flag = mysp.getBoolean("ischecked", true);
        if (flag) {
            System.out.println("22222");
            usr.setText(mysp.getString("default_username", null));
            password.setText(mysp.getString("default_password", null));
        }

        //注册部分
        bt_logon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                String in_usename = usr.getText().toString();
                String in_password = password.getText().toString();

                if (in_password.equals("")||in_usename.equals("")) {
                    Toast.makeText(getApplicationContext(), "用户名或者密码未填写", Toast.LENGTH_LONG).show();
                } else if (mysp.getString("username"+in_usename, "").equals("")) {
                    SharedPreferences.Editor editor = mysp.edit();
                    editor.putString("username"+in_usename, in_usename);
                    editor.putString("password"+in_usename, in_password);
                    editor.commit();
                    Toast.makeText(getApplicationContext(), "注册成功", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "用户名已经存在", Toast.LENGTH_LONG).show();
                }


            }
        });
        // 登陆部分
        bt_land.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                String in_usename = usr.getText().toString();
                String in_password = password.getText().toString();
//				System.out.println(mysp.getString("password", null)+"\n"+in_password);
                if (in_password.equals(mysp.getString("password"+in_usename, null))&&in_usename.equals(mysp.getString("username"+in_usename, ""))) {
                    SharedPreferences.Editor editor = mysp.edit();
                    //记住账号密码
                    if (remember.isChecked()) {
                        editor.putString("default_username",in_usename );
                        editor.putString("default_password", in_password);
                        editor.putBoolean("ischecked",true);

                    } else {
                        editor.putString("default_username",null );
                        editor.putString("default_password", null);
                        editor.putBoolean("ischecked", false);

                    }
                    editor.commit();
                    Intent intent = new Intent();
                    intent.putExtra("username", in_usename);
                    intent.setClass(register.this, MainActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "用户名或者密码错误", Toast.LENGTH_LONG).show();
                }


//				if (in_password!=""&&in_usename!="") {
//
//				}
            }
        });

    }

}